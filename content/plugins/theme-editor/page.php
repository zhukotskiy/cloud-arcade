<?php

$file_list = array();
$files = scan_files(TEMPLATE_PATH);
foreach ($files as $file) {
	$format = pathinfo($file)['extension'];
	if($format == 'php' || $format == 'html' || $format == 'js' || $format == 'css'){
		if(!strpos($file, '.min.')){
			$file_list[] = str_replace(TEMPLATE_PATH,'',$file);
		}
	}
}
$editor_mode = 'application/x-httpd-php';
$format =  isset($_POST['file_path']) ? pathinfo($_POST['file_path'])['extension'] : 'php';

if($format == 'js'){
	$editor_mode = 'javascript';
} elseif($format == 'css'){
	$editor_mode = 'css';
}

?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.59.1/codemirror.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.59.1/theme/ayu-mirage.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.59.1/codemirror.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.59.1/addon/edit/matchbrackets.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.59.1/mode/htmlmixed/htmlmixed.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.59.1/mode/xml/xml.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.59.1/mode/javascript/javascript.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.59.1/mode/css/css.js"></script>
<?php

if($format == 'php'){
	?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.59.1/mode/clike/clike.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.59.1/mode/php/php.js"></script>
	<?php
}

?>
<div class="section">
	<form action="dashboard.php?viewpage=plugin&name=theme-editor" method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label for="file_path">Select file:</label>
			<select class="form-control" name="file_path" id="file_path">
				<?php
				$current = isset($_POST['file_path']) ? $_POST['file_path'] : '';
				foreach ($file_list as $file) {
					$selected = '';
					if($current == $file){
						$selected = 'selected';
					}
					echo('<option value="'.$file.'" '.$selected.'>'.THEME_NAME.$file.'</option>');
				}

				?>
			</select>
		</div>
		<button type="submit" class="btn btn-primary btn-md">Edit</button>
	</form>
	<?php

	if(isset($_POST['file_path'])){
		if(file_exists(ABSPATH.TEMPLATE_PATH.$_POST['file_path'])){
			if(isset($_POST['action']) && isset($_POST['content']) && $_POST['action'] == 'update_file'){
				file_put_contents(ABSPATH.TEMPLATE_PATH.$_POST['file_path'], $_POST['content']);
			}
			?>
			<br>
			<form action="dashboard.php?viewpage=plugin&name=theme-editor" method="post" enctype="multipart/form-data">
				<input type="hidden" name="action" value="update_file">
				<input type="hidden" name="file_path" value="<?php echo $_POST['file_path'] ?>">
				<div class="form-group">
					<textarea class="form-control" id="editor" name="content" rows="5"><?php echo file_get_contents(ABSPATH.TEMPLATE_PATH.$_POST['file_path']) ?></textarea>
				</div>
				<button type="submit" class="btn btn-primary btn-md">Save</button>
			</form>
			<?php
		}
	}

	?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		if($("#editor").length){
			var cm = new CodeMirror.fromTextArea(document.getElementById("editor"), {
				lineNumbers: true, 
				mode: "<?php echo $editor_mode ?>",
				theme: "ayu-mirage",
				styleActiveLine: true,
				height: 20,
    			matchBrackets: true,
    			startOpen: true
			});
			cm.setSize(null, 600);
		}
	});
</script>